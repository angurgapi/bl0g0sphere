import actions from '@/store/actions'
import mutations from '@/store/mutations'

export const state = () => ({
  envData: {
    tinymce: process.env.TINYMCE
  },
  user: null
})

export { mutations, actions }
