export default {
  setCurrentUser(state, user) {
    state.user = user
      ? {
          ...user,
          photo: user.avatarUrl ? user.avatarUrl : '/img/image-empty.jpg'
        }
      : null
  },

  CLEAR_USER(state) {
    state.user = null
  }
}
